const path = require('path');
const { createFilePath } = require('gatsby-source-filesystem');

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` });
    const type = slug.startsWith('/blog/') ? 'blog' : 'page';
    createNodeField({
      node,
      name: 'slug',
      value: slug
    });
    createNodeField({
      node,
      name: 'type',
      value: type
    });
  }
};

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  return graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `).then(result => {
    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
      createPage({
        path: node.fields.slug,
        component: getComponentTemplate(node),
        context: {
          slug: node.fields.slug
        }
      });
    });
  });
};

const getComponentTemplate = node => {
  const type = node.fields.slug.startsWith('/blog/') ? 'blog' : 'page';
  return path.resolve(`./src/templates/${type}Template.tsx`);
};
