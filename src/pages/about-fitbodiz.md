---
title: 'About'
description: 'Find out more about our Personal Trainer programmes at Fiz Bodiz. Come and join us and let get Fit Bodiz together.'
heading: 'About Fit Bodiz'
image: 'AdobeStock_95728156.jpg'
---

Hi, my name is Sarah Bickford-Yates and I run Fit Bodiz. I am a Fully Qualified Personal Trainer based in Garstang Lancashire providing unique one to one [Personal Training Programmes](./personal-trainer) to suite everybody.

I love every form of exercise from running to weight training. I want to share the great feeling of exercise with you and want people to come to Fit Bodiz to get fit, toned, lose weight, be motivated and feel healthier. I will fit in around your busy life styles and budget. The feeling you get from doing exercise is like no other, it puts a smile on your face, an ache in your muscles and a body you want to show to the world every single day.

<div style="display: flex; justify-content: center;">
<img src="/img/hfe-endorsed-instructor.png" class="mt-2 mb-2" style="width: 50%;" alt="Fully Qualified in Pre and Post Natal Exercise and Nutrition">
</div>

I am a fully qualified Personal Trainer through the [Future Fit Training](http://www.futurefit.co.uk/personal-training/home/) ['Full Immersion Course.'](http://www.futurefit.co.uk/personal-training/diplomas/total-immersion-diploma)

My qualifications to date include.

- Level 2, 3 and 4 Fitness Instructor
- Exercise Programming and Coaching
- Functional Equipment Training
- Suspension Exercise Training
- Core Training
- Circuit Training
- Advanced Anatomy & Physiology
- Complete Kettlebell Trainer
- Indoor Cycling
- Nutrition & Weight Management
- Pre and Post Natal Exercise and Nutrition Qualification

I am also a mum with 4 children so I fit all this in around looking after them, running my own businesses and studying for my Level 4 Fitness Instructor.

![Personal Training Qualifications](/img/Certificates.jpg)
<br/>

<div style="display: flex; justify-content: center;">
<img src="/img/ante_and_post_natal.jpg" class="mt-2 mb-2" style="width: 50%;" alt="Fully Qualified in Pre and Post Natal Exercise and Nutrition">
</div>

## Come and join me and let's get Fit Bodiz together.

##### Contact me, Sarah, at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.

<br />
