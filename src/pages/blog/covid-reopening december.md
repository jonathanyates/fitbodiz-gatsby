---
title: 'We`re open for business yet again!'
description: 'We`re open for business yet again!'
author: 'Sarah Bickford-Yates'
date: '2020-12-02'
image: 'SocialDistancing.png'
---

A big thank you to all those who attended my free Zoom classes during November Lockdown. I am pleased to announce that I am able to provide One-To-One Personal Training with social distancing to help us all get back in shape. Sessions will be conducted with at least 1m social distancing and 2m where possible, hand sanitiser with be provided and face masks available on request.

All equipment is rigorously cleaned between sessions to ensure the safety of all clients.

<br/>

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.

<br />
<h3 style="text-align: center;">Let's get Fit Bodiz together</h3>
