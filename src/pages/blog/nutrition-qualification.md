---
title: 'Nutrition and Weight Management Qualification'
description: 'Fully Qualified in Nutrition and weight management'
author: 'Sarah Bickford-Yates'
date: '2020-06-24'
image: 'AdobeStock_74438886.jpg'
---

I am very excited to announce that I have finally passed my <b><a href="https://www.futurefit.co.uk/fitness-courses/nutrition-courses/nutrition-cpd-courses/nutrition-and-weight-management/" target="_blank">Nutrition & Weight Management course</a></b> through Future Fit, which I have been studying for longer than I can remember! This enables me to provide dietary advice to clients and deliver professional weight management as an essential addition to my personal training service.

This course is certified by the Association for Nutrition at Level 4 of the Health & Social Care Competency Framework. It has provided me with an extensive understanding of the principles of nutrition and weight management and the science that underpins it.

As a fitness professional, this is a valuable addition to my knowledge base and skillset allowing me to offer more focused nutrition and weight management sessions and advice to clients.

<div style="display: flex; justify-content: center;">
<img src="/img/nutrition-certificate.jpg" class="mt-2 mb-2" style="width: 50%;" alt="Nutrition and Weight Management Qualification">
</div>

<br/>

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.
