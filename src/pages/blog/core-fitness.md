---
title: "Core Fitness Training"
description: "Do you want that flat stomach? Your core muscles are almost like the foundations to your body. Come and join us at Fit Bodiz and lets shape up that core."
author: "Sarah Bickford-Yates"
date: "2018-12-28"
image: "AdobeStock_76490001.jpg"
---
Who doesn’t want a strong core with a flat stomach! Your core muscles are almost like the foundations to your body, even if you don’t do “core specific he“exercises you are most definitely going to be using them whilst your doing any form of exercises.

You have many muscles within your core and they are the stabilisers of your body. They are there to protect your internal organs, keep you upright and to resist against gravitational force. They are pretty important muscles of your body and they need to be exercised and strengthened daily if you can.

If you exercise them correctly the benefits out weigh the pain your may feel from focusing on them during a core class. They are the only muscles that don’t need a day or two to recover they are “hardcore” muscles and love to be worked.

If you do the exercises correctly and remember to listen to me as I prompt you during the class the out come will be a flat stomach and a six pack, and for us ladies an efficient pelvic floor! You can cheat during your core exercise by relaxing but what’s the point, you want maximum results for maximum effort.

Core exercises aren’t boring and can be a work out in themselves. 30 minutes a day is enough and the results will be plentiful.

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more.