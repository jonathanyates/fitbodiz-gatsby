---
title: 'Weights are the way to go!!'
description: ''
author: 'Sarah Bickford-Yates'
date: '2024-06-28'
image: 'AdobeStock_76490001.jpg'
---

##Don’t underestimate the power of weights!

You may be under the impression that to lose weight you need to do lots of cardio exercises that make you sweat and get out of breath or you won’t lose weight.

You may be under the impression that lifting weights is never going to improve your cardio fitness.

You are kind of right but also need to look at weights as strength and cardio. Honestly the number of clients who come to me who have never done weight training before and worry because they are going to be doing more weight training than cardio and think aren’t going to lose any weight.

Women in particular worry that they are going to turn out looking like the world’s strongest man or woman if they lift weights.

Honestly, once they get into weight training, they soon comment on how their heart rate is elevated and they feel as if they are working really hard!

<div style="display: flex; justify-content: center;">
<img src="/img/AdobeStock_10608739.jpg" class="mt-2 mb-2" style="width: 80%;" alt="Fully Qualified in Pre and Post Natal Exercise and Nutrition">
</div>

Weight training is amazing for turning fat into lean muscle mass. Helping you to lose weight and inches.

Don’t forget your heart is a muscle to so needs exercising and weight training is a low impact way of doing this without running a 10k and putting so much impact on your joints.

Weight training increases the synovial fluid (your shock absorber) between your joints so that when you are resting at night the synovial fluid sacks fill back up to protect your joints and reduce your risk of injury.

Weight training strengthens your muscles to improve your posture and physical appearance. It also reduces women’s risk to osteoporosis.

If you have quite a sedentary job, weight training combined with core exercises will strengthen your Abdominals to provide strong foundations for your spine and reduce any back issues you may have or may get as you get older.

Weight training is amazing at increasing your cardio output and helping you to burn that fat to become a leaner fitter you.

Don’t ever be scared to lift weights, if you have the correct support from a qualified person you will fall in love with weights.

Why not get in touch about Personal Training sessions with me.

<br/>

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.
