---
title: "Circuit Training"
description: "Circuit training is a good combination of body weight, weighted and high intensity exercises. What more could you ask for. Join us today."
author: "Sarah Bickford-Yates"
date: "2018-08-02"
image: "AdobeStock_101043172.jpg"
---
<div class="text-center">

#### A great way to get fit, get toned, burn fat and have fun

</div>

Circuit training is a number of exercises arranged consecutively so that each one of you can work at your own pace within the time set for the exercises. Once you have done that exercise you move onto the next one.

Circuit training is great because you can work as hard as you want with no pressure of having to keep up with the instructor, as you would have to in an exercise class to music.

Circuit training is a good combination of body weight exercises, weighted exercise and high intensity exercises.

With our circuit training there will be a combination of the different exercises at each station and a pulse raiser and core exercises at the end just to give your body that whole body workout. The exercises will be mixed up every time so that you don’t get bored with the same ones and your body is constantly being challenged.

Circuit training is al about burning calories, improving flexibility, burning fat and toning up. It’s also a great teamwork out, where everyone can encourage each other to work that bit harder. What more could you ask for!

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more.
