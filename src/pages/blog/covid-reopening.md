---
title: 'We`re open for business again'
description: 'We`re open for business again'
author: 'Sarah Bickford-Yates'
date: '2020-07-25'
image: 'AdobeStock_79367708.jpg'
---

The last few months have been a difficult time for all of us and keeping fit has been a real challenge. I am now pleased to announce that I am able to provide Personal Training sessions with social distancing to help us all get back in shape. Sessions will be conducted with at least 1m social distancing and 2m where possible, hand sanitiser will be provided and face masks available on request.

All equipment is rigorously cleaned between sessions to ensure the safety of all clients.

<div style="display: flex; justify-content: center;">
<img src="/img/SocialDistancing.png" class="mt-2 mb-2" style="width: 50%;" alt="Nutrition and Weight Management Qualification">
</div>

<br/>

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.

<br />
<h3 style="text-align: center;">Let's get Fit Bodiz together</h3>
