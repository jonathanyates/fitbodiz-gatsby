---
title: "Announcing Fit Bodiz New Gym"
description: "Announcing Fit Bodiz New Gym Facilities, built for built for personal training sessions and small classes."
author: "Sarah Bickford-Yates"
date: "2019-04-04"
image: "IMG_0121.jpg"
---
I am very excited to announce Fit Bodiz new [Gym Facilities](/gym-facilities). Purpose built for personal training sessions and small classes, providing a wide range of weights, fitness equipment, cardio machines and plenty of training space. It's a great space to get fit, lose weight, get toned and have fun.

As a [Fully Qualified Personal Trainer](/personal-trainer) I offer one-to-one or group Personal Training Programmes to suite everybody. My personal training sessions and classes cover a wide variety of fitness programs designed to help you reach your goals and push you to the best of your abilities.

Come and join me, Sarah, for training sessions in this great new facility and let's get Fit Bodiz together.

<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <img src="/img/IMG_0122.jpg" class="mt-2 mb-2" alt="Fit bodiz Gym">
    </div>
    <div class="col-sm-6">
      <img src="/img/IMG_0127.jpg" class="mt-2 mb-2" alt="Gym">
    </div>
    <div class="col-sm-6">
      <img src="/img/IMG_0126.jpg" class="mt-2 mb-2" alt="Gym Facilities">
    </div>
    <div class="col-sm-6">
      <img src="/img/IMG_0129.jpg" class="mt-2 mb-2" alt="Personal Training Gym">
    </div>
    <div class="col-sm-6">
      <img src="/img/IMG_0124.jpg" class="mt-2 mb-2" alt="Gym Equipment">
    </div>
    <div class="col-sm-6">
      <img src="/img/IMG_0132.jpg" class="mt-2 mb-2" alt="Cardio Gym Equipment">
    </div>
  </div>

</div>
<br/>

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.
