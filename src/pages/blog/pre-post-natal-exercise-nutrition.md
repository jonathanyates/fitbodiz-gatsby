---
title: 'Pre and Post Natal Exercise and Nutrition'
description: 'Fully Qualified in Pre and Post Natal Exercise and Nutrition'
author: 'Sarah Bickford-Yates'
date: '2023-03-01'
image: 'PreAndPostNatal2.jpg'
---

I am very excited to announce that I am now fully qualified in Pre and Post Natal Exercise and Nutrition. If you are trying for a baby, pregnant or post natal get in touch. Exercise is an amazing way to keep fit before, during and after birth. Helps with your birth and recovery, mentally and physically.

I am a mum of four and exercised before during and after birth, so I know first hand how important exercise is during this time. Don't be scared to exercise during pregnancy, or afterwards, I am qualified now to guide you through the pregnancy and post natal period.
Get in touch to find out more about Personal Training sessions with me. Get ready for this new chapter in your life

There are many benefits to exercise during pregnancy and after the birth, here are some of them:
Pre-natal -

- Helps to reduce high blood pressure
- Improves sleep
- Improves mood
- Improves fitness
- Helps to prevent gestational diabetes
- Reduces the incidence/severity of back pain
- Reduces the risk of urinary incontinence.... plus many more!
  Post-natal -
- Correction of muscle imbalances
- Improved posture
- increased strength and endurance for performing functional tasks.
- Improves aerobic fitness
- Improves circulation and healing.... plus many more!

<div style="display: flex; justify-content: center;">
<img src="/img/hfe-endorsed-instructor.png" class="mt-2 mb-2" style="width: 50%;" alt="Fully Qualified in Pre and Post Natal Exercise and Nutrition">
</div>

<div style="display: flex; justify-content: center;">
<img src="/img/ante_and_post_natal.jpg" class="mt-2 mb-2" style="width: 50%;" alt="Fully Qualified in Pre and Post Natal Exercise and Nutrition">
</div>

<br/>

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.
