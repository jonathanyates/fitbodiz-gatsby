---
title: 'Announcing Fitbodiz New Fitness Videos'
description: 'Announcing Fitbodiz New Fitness Videos'
author: 'Sarah Bickford-Yates'
date: '2020-03-28'
image: 'CoreFitnessThumbnail.jpg'
---

We are currently all living through very difficult times due to the Coronavirus. Being able to keep ourselves fit and
health has never been more challenging. To help us all keep fit and healthly I am now posting fitness videos on the <a href="https://www.youtube.com/channel/UCviMBl8-ye-aPXwiAHGM2lA" target="_blank">
<b> The Fit Bodiz Fitness Videos Channel </b>
</a> so that you can do my fitness sessions from home.

Checkout the <a href="https://www.youtube.com/channel/UCviMBl8-ye-aPXwiAHGM2lA" target="_blank">
<b> The Fit Bodiz Fitness Videos Channel </b>
</a> on
youtube and lets get fitbodiz together.
