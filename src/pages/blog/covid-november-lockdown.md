---
title: 'Free ZOOM classes during November Lockdown'
description: 'Free Zoom classes during November Lockdown'
author: 'Sarah Bickford-Yates'
date: '2020-11-05'
image: 'zoom.jpg'
---

<p>
Due to COVID-19 lockdown restrictions we are unable to provide our service as normal. This is yet another very challenging
time for us all, with fitness facilities and services having to close at a time when we most need them.
</p>
<p>
To help get us all through this Fit Bodiz is providing FREE training sessions via ZOOM until the end of this lockdown
period, which is currently due to end on 2nd December. Sessions will include HIIT and Aerobic classes aimed at all levels
of fitness. Further session details will be announced throughout this period.
</p>

##### Please contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) for details on how to join my Zoom classes.

<br />
<h3 style="text-align: center;">Let's get Fit Bodiz together</h3>
