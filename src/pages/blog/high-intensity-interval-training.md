---
title: "High Intensity Interval Training"
description: "This is a High Intensity Interval Training class. It’s fast and progressive using your body weight and all your energy to burn that body fat and calories."
heading: "HIIT - Body Burner"
author: "Sarah Bickford-Yates"
date: "2018-10-20"
image: "AdobeStock_92072408.jpg"
---
This is a High Intensity Interval Training class. It’s fast and progressive using your body weight and all your energy to burn that body fat and calories.

You can work at your own pace on each exercise as long as you change the exercise when instructed. The harder you work the greater the rewards. A leaner, more toned body and less body fat.

The great thing about a HIIT style workout is that its fast, which means you, won’t have time to get bored. You will be shattered at the end but trust me you will be buzzing. Your heart rate will be fast which means your body is releasing metabolic chemicals to burn fats and use them for the energy. Even after the class your body will still be burning fat. Its a win, win!

It’s a great way to improve your overall fitness, coordination, get toned, and loose the body fat.

With all our classes you can work at your own pace and each time you do it push yourself that bit harder. The harder you work the greater the changes to your body.

Come on lets get FIT BODIZ!

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more.