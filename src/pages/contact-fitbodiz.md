---
title: "Contact Us"
description: "Contact us at Fit Bodiz and find our more about our Personal Training Programmes. Find us on Facebook and give us a call today and lets get Fit Bodiz"
heading: "Contact Us"
image: "AdobeStock_105847359.jpg"
---
We'd love to hear from you. Please contact Sarah at Fit Bodiz if you'd like to find out more about our Personal Training Programmes.

<div class="container">

<div class="row">

<div class="col-md-6">

##### Fit Bodiz

*   21 Topiary Gardens
*   Garstang
*   Lancashire
*   PR3 1YF

</div>

<div class="col-md-6">

##### Contact

*   Email [info@fitbodiz.com](mailto:info@fitbodiz.com)
*   Email [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com)

</div>

</div>

</div>
