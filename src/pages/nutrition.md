---
title: 'Nutrition'
description: 'Personal Training and Nutrition & Weight Management programmes at Fiz Bodiz.'
heading: 'Nutrition & Weight Management'
image: 'AdobeStock_74438886.jpg'
---

As a PT I am also fully qualified in Nutrition & Weight Management. This enables me to provide dietary advice to you and deliver professional weight management as an essential addition to my personal training service.

I have gained this qualification through the <b><a href="https://www.futurefit.co.uk/fitness-courses/nutrition-courses/nutrition-cpd-courses/nutrition-and-weight-management/" target="_blank">Future Fit Nutrition & Weight Management</a></b> course. This course is certified by the Association for Nutrition at Level 4 of the Health & Social Care Competency Framework. It has provided me with an extensive understanding of the principles of nutrition and weight management and the science that underpins it.

As a fitness professional, this is a valuable addition to my knowledge base and skillset allowing me to offer more focused nutrition and weight management sessions and advice to clients.

<div style="display: flex; justify-content: center;">
<img src="/img/nutrition-certificate.jpg" class="mt-2 mb-2" style="width: 50%;" alt="Nutrition and Weight Management Qualification">
</div>

## Come and join me and let's get Fit Bodiz together.

##### Contact me, Sarah, at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.

<br />
