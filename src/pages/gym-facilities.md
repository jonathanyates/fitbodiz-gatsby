---
title: 'Gym Facilities'
description: 'Gym Facilities in Garstang. Whether you want to lose weight or get fit join us today for one-to-one Personal Training Programmes.'
image: 'IMG_0122.jpg'
---

I am very excited to announce Fit Bodiz new Gym Facilities. Purpose built for personal training sessions and small classes, providing a wide range of weights, fitness equipment, cardio machines and plenty of training space. It's a great space to get fit, lose weight, get toned and have fun.

![Garstang Gym Facilities](/img/IMG_0121.jpg)

As a Fully Qualified Personal Trainer I offer one-to-one or group Personal Training Programmes to suite everybody. My personal training sessions and classes cover a wide variety of fitness programs designed to help you reach your goals and push you to the best of your abilities.

Come and join me, Sarah, for training sessions in this great new facility and let's get Fit Bodiz together.
<br />

![Gym Facilities](/img/IMG_0126.jpg)
<br />
<br />
![Personal Training Gym](/img/IMG_0129.jpg)
<br />
<br />
![Gym Equipment](/img/IMG_0124.jpg)
<br />
<br />
![Cardio Gym Equipment](/img/IMG_0132.jpg)

<br />

##### Contact me at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.

<br/>
