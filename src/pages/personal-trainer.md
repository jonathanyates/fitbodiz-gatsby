---
title: 'Fully Qualified Personal Trainer'
description: 'One to One Personal Training programmes to suite every body. Whether you want to lose weight or get fit join us today for one-to-one Personal Training Programmes.'
heading: 'Personal Trainer'
image: '/carousel/AdobeStock_10608739.jpg'
---

As a Fully Qualified Personal Trainer I offer one-to-one or group Personal Training Programmes to suite everybody.

Whether you want to lose weight or get fit I will help you as your Personal Trainer to reach your goals and push you to the best of your ability.

I am a fully qualified Personal Trainer through [Future Fit Training Ltd](http://www.futurefit.co.uk/personal-training/home/) studying their [Full Immersion Course.](http://www.futurefit.co.uk/personal-training/diplomas/total-immersion-diploma)

My qualifications to date include.

- Level 2, 3 and 4 Fitness Instructor
- Exercise Programming and Coaching
- Functional Equipment Training
- Suspension Exercise Training
- Core Training
- Circuit Training
- Advanced Anatomy & Physiology
- Complete Kettlebell Trainer
- Indoor Cycling

![Personal Training Qualifications](/img/Certificates.jpg)
<br/>

<div style="display: flex; justify-content: center;">
<img src="/img/ante_and_post_natal.jpg" class="mt-2 mb-2" style="width: 50%;" alt="Fully Qualified in Pre and Post Natal Exercise and Nutrition">
</div>

##### Please contact me, Sarah, at [sarah@fitbodiz.com](mailto:sarah@fitbodiz.com) to find out more how I can help you achieve your goals.

<br />
