---
title: "Nutritional Supplements"
description: "Fit Bodiz offers Nutritional Supplements by Proto-col"
---
 <div>
  <img src="/img/proto-col/proto-col.png" WIDTH="300px" class="mx-auto img-responsive z-depth-0 mb-2">
</div>
 <p>
    Nutritional Supplements are an important part of your daily training.
    Fit Bodiz has teamed up with <a href="https://www.proto-col.com/naturally-advanced-nutrition.html" target="_blank">Proto-col</a> to offer you all the essential nutritional supplements you need to
    train effectively to keep fit and heathly.
  </p>
  <p>
    Proto-col naturally advanced nutritional supplements have been specifically designed to offer you the
    correct balance of nutrients, vitamins, minerals and antioxidants. We offer a range of naturally advanced
    nutritional food supplements designed to maintain optimum health and well-being, as well as improve physical
    performance and enhance recovery. Designed to complement your healthy diet plan, proto-col has formulated
    it’s nutritional supplements to help sustain your natural vitality and well-being.
  </p>
  <div class="mt-2 mb-2">
    <div class="h5-responsive hidden-lg-down">
      <p>
        You can order your essential Nutrition Supplements at <a href="mailto:info@fitbodiz.com" target="_blank">info@fitbodiz.com</a>
      </p>
    </div>
    <div class="hidden-xl-up" style="font-weight: 500">
      <p>
        You can order your essential Nutrition Supplements at <a href="mailto:info@fitbodiz.com" target="_blank">info@fitbodiz.com</a>
      </p>
    </div>
  </div>
</div>




<div class="mb-2">
  <p class="h3-responsive">Why should you take green magic?</p>

  <p class="mb-2">The World Health Organization and the NHS recommends that you take 5 portions of fruit and vegetables a day.
              Fruit and vegetables are part of a balanced diet and can help us stay healthy, that’s why it so important
              that we get enough of them. With today's hectic lifestyles and convenient fast food many people find that
              they are not able to achieve this recommended standard – and this is where proto-col green magic comes in.
              One serving of green magic delivers approximately the same food value as 5–7 helpings of organically grown
              fresh fruit or vegetables but with just 15 calories.</p>

  <div class="row">
    <div class="col-md-4">
      <img src="/img/proto-col/Green-Magic-Capsules-285.jpg" class="img-responsive z-depth-0">
    </div>
    <div class="col-md-8">
      <p class="h4-responsive mb-2">Green Magic</p>
      <p><p>
                      Green magic is a revolutionary product that blends 16 of the world's most effective and nutritious
                      superfoods into one convenient serving. Available in both powder and capsule form - green magic is
                      an easy and convenient way of getting your 5 a day in a single dose.
                  </p>
                  <p>
                      Green magic is an excellent source of vitamins and minerals, including folate, vitamin C and potassium.
                      It is also an excellent source of dietary fibre, which helps maintain a healthy gut and prevent
                      constipation and other digestion problems.
                  </p></p>
      <ul>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Green Magic - 235 Capsules</div>
            <div>£34.95</div>
          </div>
        </li>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Green Magic - 400G</div>
            <div>£59.95</div>
          </div>
        </li>
        
      </ul>
    </div>
  </div>
</div>


<div class="mb-2">
  <p class="h3-responsive">Why you should try proto-col&#39;s pro sports plus collagen tablets?</p>

  <p class="mb-2"><p>Each collagen tablet contains 1000mg/1 gram of pure collagen hydrolysate. Our collagen is type I
            and III with an optimised type II grade. Our jar contains 150 tablets with a recommended serving of 3 to 5
            tablets per night.</p>
        <p class="mb-2">Collagen is the main structural protein of the various connective tissues in all humans and
            animals. Collagen
            is also the main component of connective tissue it is the most abundant protein in mammals, making up from
            25-35% of the whole-body protein content.</p></p>

  <div class="row">
    <div class="col-md-4">
      <img src="/img/proto-col/collagen-pro-sports-150-tablets.jpg" class="img-responsive z-depth-0">
    </div>
    <div class="col-md-8">
      <p class="h4-responsive mb-2">Collagen Pro Sports Plus</p>
      <p>Proto-col collagen is approved by the CRI and is one of the only brands of collagen hydrolysate on
                  the market with both beauty claims which have been cleared by FSE Food Supplement Europe (see our
                  beauty collagen capsules type I and III) and claims under review by EFSA for our sports collagen
                  capsules and tablets which are type I and III with an optimised type II collagen. This makes our
                  product the most targeted and results-based collagen on the market.</p>
      <ul>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Collagen Pro Sports Plus - 150 tablets</div>
            <div>£39.95</div>
          </div>
        </li>
        
      </ul>
    </div>
  </div>
</div>


<div class="mb-2">
  <p class="h3-responsive">Why you should try proto-col&#39;s Collagen Body &amp; Joint Formulation?</p>

  <p class="mb-2">Taking collagen orally in the form of collagen capsules can help rebuild the existing collagen
            stores in the body and joints as well as stimulate the body’s own production. Although tiny amounts of
            collagen are available in foods that we can buy from the supermarket, the levels of collagen required to
            maintain or rebuild collagen levels in the body can only be gained from taking a high grade of collagen such
            as that found in proto-col products.</p>

  <div class="row">
    <div class="col-md-4">
      <img src="/img/proto-col/collagen-body-and-joint-90-capsules.jpg" class="img-responsive z-depth-0">
    </div>
    <div class="col-md-8">
      <p class="h4-responsive mb-2">Collagen Body &amp; Joint Formulation</p>
      <p>There are 20 kinds of amino acids found in our bodies. These are made up of essential and
                    non-essential amino acids. Essential amino acids mean that they should be an essential part of your
                    diet as they are not naturally produced by our bodies. Non-essential amino acids are naturally
                    produced by our bodies so you don’t need to consume them.</p>
      <ul>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Collagen Body &amp; Joint Formulation</div>
            <div>£29.95</div>
          </div>
        </li>
        
      </ul>
    </div>
  </div>
</div>


<div class="mb-2">
  <p class="h3-responsive"></p>

  <p class="mb-2"></p>

  <div class="row">
    <div class="col-md-4">
      <img src="/img/proto-col/calcium-complex.jpg" class="img-responsive z-depth-0">
    </div>
    <div class="col-md-8">
      <p class="h4-responsive mb-2">Calcium Complex</p>
      <p><p>
                    Calcium is one of the key minerals that our body requires to function. One of the most abundant
                    minerals in the body, it provides many health benefits including strong bones, nails and teeth. Take
                    proto-col calcium complex to supplement your daily calcium levels.
                  </p>
                  <p>
                    Osteoporosis is a huge health concern as we age, bones become weak and brittle making them
                    susceptible to breaks and fractures. Protect yourself and strengthen your bones, nails and teeth
                    with proto-col calcium complex.
                  </p></p>
      <ul>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Calcium Complex</div>
            <div>£9.95</div>
          </div>
        </li>
        
      </ul>
    </div>
  </div>
</div>


<div class="mb-2">
  <p class="h3-responsive">Why is Pro Gold so powerful?</p>

  <p class="mb-2">Pro Gold has been scientifically formulated to combine Whey protein and Collagen protein in a unique, powerful performance and recovery product. Each tasty 25g serving delivers 20g of protein. The Whey protein is an 80% concentrate and the Collagen protein is a 97% pure protein. There are two great flavours to choose from, Strawberry or Chocolate. Pro Gold is packed full of Essential Amino Acids (EAAs), which are required by the body to build new tissue and help with cellular repair that takes place when muscles are recovering from exercise.</p>

  <div class="row">
    <div class="col-md-4">
      <img src="/img/proto-col/pro-gold-chocolate-1KG.jpg" class="img-responsive z-depth-0">
    </div>
    <div class="col-md-8">
      <p class="h4-responsive mb-2">Pro Gold Whey &amp; Collagen Protein</p>
      <p>
<p>
Getting the right amount of protein, EAAs and BCAAs within a diet can help athletes to improve their physique and also recover quicker from exercise. Muscle is actually broken down during exercise and workouts. During the recovery period, your body uses protein to repair the muscle fibres. The proteins in Pro Gold are absorbed into your system to support recovery and aid muscle growth. Pro Gold collagen protein also strengthens your ligaments, tendons and connective tissue, making Pro Gold the most powerful protein blend in the market.
</p>
<p>
Pro Gold is best enjoyed first thing in the morning and/or during the 30 minutes just before or immediately after training. Mix one scoop with 150-250ml of your favourite beverage in either milk or water in your proto-col shaker. Try adding a spoonful of green magic to give you a fantastic power shake. Use 1-4 times daily or as required.
</p>
</p>
      <ul>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Pro Gold Whey &amp; Collagen Protein - Chocolate</div>
            <div>£39.95</div>
          </div>
        </li>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Pro Gold Whey &amp; Collagen Protein - Strawberry</div>
            <div>£39.95</div>
          </div>
        </li>
        
      </ul>
    </div>
  </div>
</div>


<div class="mb-2">
  <p class="h3-responsive"></p>

  <p class="mb-2"></p>

  <div class="row">
    <div class="col-md-4">
      <img src="/img/proto-col/Vitamin-D-90-Capsules.jpg" class="img-responsive z-depth-0">
    </div>
    <div class="col-md-8">
      <p class="h4-responsive mb-2">Vitamin D Tablets</p>
      <p>
<p>
Each proto-col vitamin D tablet contains 5000 international units.  Vitamin D is a fat-soluble vitamin and is unique because it functions as a hormone whilst the body synthesises the vitamin from adequate sun exposure. With a lack of sunlight people can become deficient therefore it may be necessary to take vitamin D as a food supplement. At proto-col we use vitamin D3 in this supplement which is the real vitamin D, as it is what is synthesised naturally in our skin from exposure to the sun. This type of vitamin D has a greater absorbency rate within the human body and is an essential vitamin only found in very limited quantities of natural food sources.
</p>
<p>
Recent studies have shown that sufficient levels of vitamin D can have positive effects on numerous vital body functions including: strength and power, immune function and the prevention of common allergies.
</p></p>
      <ul>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Vitamin D Tablets</div>
            <div>£11.95</div>
          </div>
        </li>
        
      </ul>
    </div>
  </div>
</div>


<div class="mb-2">
  <p class="h3-responsive"></p>

  <p class="mb-2"></p>

  <div class="row">
    <div class="col-md-4">
      <img src="/img/proto-col/osteo+-90-tablets.jpg" class="img-responsive z-depth-0">
    </div>
    <div class="col-md-8">
      <p class="h4-responsive mb-2">Osteo+</p>
      <p>
<p>
The combination of ingredients provides an optimal nutrition supplement to ease joint pain and promote joint health. Scientific evidence suggests that the ingredients work together to provide a powerful therapeutic effect.
</p>
<p>
The product has impressed Professor Greg Whyte OBE PhD FACSM, Professor of Applied Health and Sports Science at Liverpool John Moores University, so much so he’s put his name to the product.
</p>
    </p>
      <ul>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Osteo+</div>
            <div>£29.95</div>
          </div>
        </li>
        
      </ul>
    </div>
  </div>
</div>


<div class="mb-2">
  <p class="h3-responsive"></p>

  <p class="mb-2"></p>

  <div class="row">
    <div class="col-md-4">
      <img src="/img/proto-col/omega-120-capsules.jpg" class="img-responsive z-depth-0">
    </div>
    <div class="col-md-8">
      <p class="h4-responsive mb-2">Omega Essentials 3, 6 &amp; 9</p>
      <p>Omega-3 fatty acids are thought to help reduce the risk of heart disease and also to promote healthy skin. They are also used along with diet and exercise to help lower levels of a certain blood fat (triglyceride) and to raise levels of "good" cholesterol (HDL). This product may also be used to help treat high blood pressure or rheumatoid arthritis.</p>
      <ul>
        
        <li>
          <div class="font-weight-bold d-flex justify-content-end">
            <div class="mr-auto">Omega Essentials 3, 6 &amp; 9</div>
            <div>£12.95</div>
          </div>
        </li>
        
      </ul>
    </div>
  </div>
</div>




<div class="h5-responsive">
  <p>
    You can order your essential Nutrition Supplements at <a href="mailto:info@fitbodiz.com" target="_blank">info@fitbodiz.com</a>
  </p>
</div>
