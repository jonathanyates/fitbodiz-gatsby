import React from 'react';

export const Heading = ({ text }) => <h1 className="section-heading mb-2 mt-0">{text}</h1>;
