import React from 'react';

export const InlineLink = ({ href, text }) => (
  <React.Fragment>
    {' '}
    <a href={href} target="_blank">
      {text}
    </a>{' '}
  </React.Fragment>
);
