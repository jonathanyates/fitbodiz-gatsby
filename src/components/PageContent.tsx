import React from 'react';

const PageContent = ({ content }) => (
  <div className="excerpt">
    <div dangerouslySetInnerHTML={{ __html: content }} />
  </div>
);

export default PageContent;
