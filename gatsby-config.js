module.exports = {
  siteMetadata: {
    siteUrl: 'https://www.fitbodiz.com',
    title: `Fit Bodiz - Fully Qualified Personal Trainer in Garstang`,
    description:
      'Fitbodiz is run by Sarah Bickford-Yates, a Fully Qulified Personal Trainer based in Garstang, Lancashire, offering professional & friendly one-to-one and group Personal Training Sessions.',
    author: 'Fit Bodiz'
  },
  plugins: [
    'gatsby-plugin-typescript',
    'gatsby-plugin-tslint',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`
      }
    },
    `gatsby-transformer-remark`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    'gatsby-plugin-robots-txt',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Fit Bodiz',
        short_name: 'Fitbodiz',
        start_url: '/',
        background_color: '#ffffff',
        theme_color: '#4285f4',
        display: 'standalone'
      }
    },
    'gatsby-plugin-offline',
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: ['G-ZCLM235QZW', 'UA-101841341-1', 'G-LKPQS96VNJ'],
        // This object gets passed directly to the gtag config command
        // This config will be shared across all trackingIds
        gtagConfig: {
          optimize_id: 'OPT_CONTAINER_ID',
          anonymize_ip: true,
          cookie_expires: 0
        },
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: false,
          // Setting this parameter is also optional
          respectDNT: true,
          // Avoids sending pageview hits from custom paths
          exclude: ['/preview/**', '/do-not-track/me/too/']
        }
      }
    }
  ]
};
